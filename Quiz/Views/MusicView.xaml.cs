﻿using Quiz.Helpers;
using Quiz.Models;
using Quiz.Windows;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz.Views
{
    public partial class MusicView : UserControl
    {
        Window window;
        MusicQuestion viewModel;
        ProgressBar bar;
        double percent;

        public MusicView()
        {
            InitializeComponent();

            Loaded += MusicView_Loaded;
            Unloaded += MusicView_Unloaded;
            DataContextChanged += MusicView_DataContextChanged;
        }

        private void MusicView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (viewModel != null)
                viewModel.Dispose();

            viewModel = DataContext as MusicQuestion;

            if (viewModel != null)
                viewModel.Init(Player);
        }

        private void MusicView_Loaded(object sender, RoutedEventArgs e)
        {
            window = Window.GetWindow(this);

            window.KeyDown += Window_KeyDown;
            window.MouseWheel += Window_MouseWheel;
        }

        private void MusicView_Unloaded(object sender, RoutedEventArgs e)
        {
            window.KeyDown -= Window_KeyDown;
            window.MouseWheel -= Window_MouseWheel;
        }

        private void Window_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            var volume = viewModel.Volume;
            volume += e.Delta > 0 ? 5 : -5;

            if (volume < 0)
                volume = 0;
            else if (volume > 100)
                volume = 100;

            viewModel.SetVolume(volume);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                viewModel.ChangeStateCommand.Execute(null);
        }

        private void ProgressBar_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                bar = sender as ProgressBar;
                bar.CaptureMouse();
                viewModel.barClicked = true;
                UpdatePercent(bar.ActualWidth, e.GetPosition(bar));
            }
        }

        void UpdatePercent(double width, Point pos)
        {
            percent = pos.X / width;
            if (percent < 0)
                percent = 0;
            else if (percent > 1)
                percent = 1;
            bar.Value = percent * 100;
            viewModel.UpdateTimeText(TimeSpan.FromSeconds(Player.NaturalDuration.TimeSpan.TotalSeconds * percent));
        }

        private void ProgressBar_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (viewModel.barClicked)
                UpdatePercent(bar.ActualWidth, e.GetPosition(bar));
        }

        private void ProgressBar_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Released && viewModel.barClicked)
            {
                bar.ReleaseMouseCapture();
                viewModel.barClicked = false;
                Player.Position = TimeSpan.FromSeconds(Player.NaturalDuration.TimeSpan.TotalSeconds * percent);
            }
        }
    }
}

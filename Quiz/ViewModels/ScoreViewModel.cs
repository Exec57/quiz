﻿using Quiz.Helpers;
using Quiz.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Quiz.ViewModels
{
    public class ScoreViewModel : BaseViewModel
    {
        const string fileName = "score";

        string filter = "";
        List<Score> list;

        public string Filter { get => filter; set { filter = value; UpdateScores(); } }
        public ObservableCollection<Score> Scores { get; set; }

        public ScoreViewModel()
        {
            list = new List<Score>();
            Scores = new ObservableCollection<Score>();

            Read();
        }

        void Read()
        {
            if (File.Exists(fileName))
            {
                try
                {
                    var lines = File.ReadAllText(fileName, Encoding.Default).Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

                    foreach (var line in lines)
                    {
                        var args = line.Split(';');
                        var score = new Score
                        {
                            Pseudo = args[0],
                            ThemeName = args[1],
                            Date = args[2],
                            Value = int.Parse(args[3]),
                            Max = int.Parse(args[4])
                        };

                        var percent = score.Value / (double)score.Max;
                        score.Percent = (int)(percent * 100);

                        if (score.Percent >= 90)
                            score.Color = Brushes.DarkGreen;
                        else if (score.Percent >= 70)
                            score.Color = Brushes.Green;
                        else if (score.Percent >= 50)
                            score.Color = Brushes.Yellow;
                        else if (score.Percent >= 25)
                            score.Color = Brushes.Red;
                        else
                            score.Color = Brushes.DarkRed;

                        list.Add(score);
                    }

                    UpdateScores();
                }
                catch { }
            }
        }

        public static void Add(string pseudo, string themeName, int score, int max)
        {
            try
            {
                using (var sw = new StreamWriter(fileName, true))
                {
                    var line = $"\n{pseudo};{themeName};{DateTime.Now.ToShortDateString()};{score};{max}";
                    sw.WriteLine(line);
                }
            }
            catch { }
        }

        void UpdateScores()
        {
            var filteredList = list.Where(i => i.ToString().ToLower().Contains(Filter.ToLower())).ToList();

            Scores = new ObservableCollection<Score>(filteredList);
            OnPropertyChanged(nameof(Scores));
        }
    }
}

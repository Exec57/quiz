﻿using Lidgren.Network;
using Microsoft.Win32;
using Quiz.Helpers;
using Quiz.Models;
using Quiz.Networks;
using Quiz.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Quiz.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        const string waitingForConnectionMessage = "Attente d'une connexion...";
        const string newConnectionMessage = "{0} s'est connecté";
        const string connectedMessage = "Vous êtes connecté chez {0}";
        const string newDisconnectionMessage = "{0} s'est déconnecté";
        const string hostFailedMessage = "Impossible d'héberger";
        const string connectingMessage = "Connexion en cours...";
        const string connectionFailedMessage = "Impossible de se connecter";
        const string getQuestionsMessage = "Récupération des questions: {0} / {1}";
        const string loadingThemeMessage = "Chargement du thème...";
        const string loadedThemeMessage = "Thème chargé";
        const string errorSettingsMessage = "Erreur avec le fichier Settings.ini";

        Window window;
        object currentView;
        string message;
        string otherPseudo;
        List<Question> questions;
        bool showStartButton = false;
        string themeName;
        int maxQuestions;
        string title;

        public object CurrentView { get => currentView; set => OnPropertyChanged(ref currentView, value); }
        public string Message { get => message; set => OnPropertyChanged(ref message, value); }
        public bool ShowStartButton { get => showStartButton; set => OnPropertyChanged(ref showStartButton, value); }
        public string Title { get => title; set => OnPropertyChanged(ref title, value); }

        public ICommand NewGameCommand { get; set; }
        public ICommand GiveUpCommand { get; set; }
        public ICommand ScoreCommand { get; set; }
        public ICommand SettingsCommand { get; set; }
        public ICommand ExitCommand { get; set; }

        public ICommand NetworkHostCommand { get; set; }
        public ICommand NetworkJoinCommand { get; set; }
        public ICommand NetworkDisconnectCommand { get; set; }

        public ICommand StartCommand { get; set; }

        public MainViewModel(Window window)
        {
            this.window = window;

            questions = new List<Question>();

            window.Closing += Window_Closing;

            NewGameCommand = new RelayCommand(NewGame, () => !Network.IsConnecting && !Network.IsNoConnection);
            GiveUpCommand = new RelayCommand(GiveUp, () => GameViewModel.role != -1);
            ScoreCommand = new RelayCommand(() => new ScoreWindow().ShowDialog());
            SettingsCommand = new RelayCommand(OpenSettings, () => Network.IsValid ? false : !(CurrentView is GameViewModel));
            ExitCommand = new RelayCommand(window.Close);

            NetworkHostCommand = new RelayCommand(NetworkHost, () => !Network.IsValid);
            NetworkJoinCommand = new RelayCommand(NetworkJoin, () => !Network.IsValid);
            NetworkDisconnectCommand = new RelayCommand(Disconnect, () => Network.IsValid);

            StartCommand = new RelayCommand(() => Network.SendPacket(new StartPacket(), true));

            while (!Settings.Read())
            {
                if (File.Exists(Settings.file))
                    MessageBox.Show(errorSettingsMessage);
                Settings.Write();
                Process.Start(Settings.file).WaitForExit();
            }

            Network.OnReceivedPacket<PseudoPacket>(OnReceivedPseudo);
            Network.OnReceivedPacket<NewGamePacket>(OnReceivedNewGame);
            Network.OnReceivedPacket<NewQuestionPacket>(OnReceivedNewQuestion);
            Network.OnReceivedPacket<StartPacket>(OnReceivedStartPacket);
            Network.OnReceivedPacket<GiveUpPacket>((_) => Reset());

            UpdateTitle();
        }

        bool ShowNoDialog(string body)
        {
            var gameViewModel = CurrentView as GameViewModel;

            if (gameViewModel != null && !gameViewModel.Finish &&
                MessageBox.Show(body, "Partie en cours",
                MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
            {
                return true;
            }

            return false;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (ShowNoDialog("Etes vous sur de vouloir quitter ?"))
            {
                e.Cancel = true;
                return;
            }

            Network.Stop();

            MusicQuestion.TryStop(CurrentView);
        }

        void NewGame()
        {
            var dialog = new OpenFileDialog();
            dialog.Filter = "Zip files (*.zip)|*.zip";
            dialog.Title = "Choisir un thème";
            dialog.InitialDirectory = Utility.applicationDirectory + "\\themes";

            if (dialog.ShowDialog() == true)
                LoadTheme(dialog.FileName);
        }

        public void LoadTheme(string path)
        {
            if (ShowNoDialog("Etes vous sur de vouloir lancer une nouvelle partie ?"))
                return;

            Reset();
            Message = null;
            Task.Run(() =>
            {
                if (OpenZip(path))
                {
                    themeName = Path.GetFileNameWithoutExtension(path);
                    UpdateTitle();
                    maxQuestions = questions.Count;

                    questions.Randomize();

                    if (Network.IsValid)
                    {
                        Network.SendPacket(new NewGamePacket(themeName, maxQuestions));

                        foreach (var item in questions)
                            Network.SendPacket(new NewQuestionPacket(item));
                    }
                    else
                        ShowStartButton = true;
                }
                else
                {
                    Message = null;
                    MessageBox.Show("Erreur avec le fichier zip");
                }
            });
        }

        void NetworkHost()
        {
            if (ShowNoDialog("Etes vous sur de vouloir héberger ?"))
                return;

            Reset();
            if (Network.Host(Settings.Port))
            {
                Network.OnStatusChanged += OnNetworkStatusChanged;
                Message = waitingForConnectionMessage;
            }
            else
                Message = hostFailedMessage;
        }

        void NetworkJoin()
        {
            if (ShowNoDialog("Etes vous sur de vouloir rejoindre ?"))
                return;

            Reset();
            Message = connectingMessage;

            Network.Connect(Settings.Ip, Settings.Port);
            Network.OnStatusChanged += OnNetworkStatusChanged;
        }

        void OnNetworkStatusChanged(NetConnectionStatus status, NetConnectionStatus oldStatus)
        {
            if (status == NetConnectionStatus.Connected)
                Network.SendPacket(new PseudoPacket(Settings.Pseudo));
            else if (status == NetConnectionStatus.Disconnected)
            {
                Reset();
                Message = oldStatus == NetConnectionStatus.Connected ? string.Format(newDisconnectionMessage, otherPseudo) : connectionFailedMessage;
                Network.Stop();
                Utility.RaiseCanExecuteChanged();
            }
        }

        void Disconnect()
        {
            if (ShowNoDialog("Etes vous sur de vouloir vous déconnecter ?"))
                return;

            Reset();
            Network.Stop();
            Utility.RaiseCanExecuteChanged();
        }

        void OnReceivedPseudo(PseudoPacket packet)
        {
            otherPseudo = packet.pseudo;
            var msg = Network.IsServer ? newConnectionMessage : connectedMessage;
            Message = string.Format(msg, otherPseudo);
            Utility.RaiseCanExecuteChanged();
        }

        void OnReceivedNewGame(NewGamePacket packet)
        {
            Reset();
            maxQuestions = packet.maxQuestions;
            themeName = packet.themeName;
            UpdateTitle();
            Message = string.Format(getQuestionsMessage, 0, maxQuestions);
            GameViewModel.role = 2;
        }

        void OnReceivedNewQuestion(NewQuestionPacket packet)
        {
            questions.Add(packet.newQuestion);
            Message = string.Format(getQuestionsMessage, questions.Count, maxQuestions);

            if (questions.Count == maxQuestions)
                ShowStartButton = true;
        }

        bool OpenZip(string file)
        {
            try
            {
                Message = loadingThemeMessage;

                GameViewModel.role = Network.IsValid ? 1 : 0;

                var memory = new MemoryStream(File.ReadAllBytes(file));

                questions.Clear();
                using (var zip = new ZipArchive(memory))
                {
                    if (zip.Entries.Count == 0)
                        return false;

                    foreach (var entry in zip.Entries)
                    {
                        var stream = entry.Open();
                        int len = (int)entry.Length;
                        var buffer = new byte[len];
                        stream.Read(buffer, 0, len);
                        var question = Question.New(entry.Name, buffer);
                        if (question == null)
                            return false;
                        questions.Add(question);
                    }
                }

                Message = loadedThemeMessage;

                return true;
            }
            catch { }

            return false;
        }

        void Reset()
        {
            MusicQuestion.TryStop(CurrentView);
            Message = null;
            ShowStartButton = false;
            CurrentView = null;
            questions.Clear();
            GameViewModel.role = -1;
            themeName = "";
            UpdateTitle();
        }

        void OnReceivedStartPacket(StartPacket packet)
        {
            var pseudo = GameViewModel.role == 1 ? otherPseudo : Settings.Pseudo;

            Message = null;
            ShowStartButton = false;
            CurrentView = new GameViewModel(questions, pseudo, themeName);
        }

        void OpenSettings()
        {
            window.Hide();
            Process.Start(Settings.file).WaitForExit();
            while (!Settings.Read())
            {
                MessageBox.Show("Erreur");
                Process.Start(Settings.file).WaitForExit();
            }
            MusicQuestion.currentVolume = Settings.Volume;
            window.Show();
        }

        void GiveUp()
        {
            if (ShowNoDialog("Etes vous sur de vouloir abandonner ?"))
                return;

            Network.SendPacket(new GiveUpPacket(), true);
        }

        void UpdateTitle()
        {
            Title = "Quiz";
            if (!string.IsNullOrEmpty(themeName))
                Title += " - " + themeName;
        }
    }
}

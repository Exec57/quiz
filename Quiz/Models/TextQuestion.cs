﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz.Models
{
    public class TextQuestion : Question
    {
        double maxWidth;

        public ObservableCollection<string> Texts { get; set; }
        public double MaxWidth { get => maxWidth; set => OnPropertyChanged(ref maxWidth, value); }

        public TextQuestion(byte[] buffer) : base(buffer)
        {
            Texts = new ObservableCollection<string>();

            string text = Encoding.Default.GetString(buffer);

            var list = text.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in list)
                Texts.Add(item);
            OnPropertyChanged(nameof(Texts));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Quiz.Networks
{
    public class ScorePacket : Packet
    {
        public int delta { get; private set; }

        public ScorePacket(int delta)
        {
            this.delta = delta;
        }

        public ScorePacket()
        {
        }

        public override void Serialize(NetOutgoingMessage outmsg)
        {
            outmsg.Write(delta);
        }

        public override void Deserialize(NetIncomingMessage inc)
        {
            delta = inc.ReadInt32();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;
using Quiz.Models;

namespace Quiz.Networks
{
    public class NewQuestionPacket : Packet
    {
        static Dictionary<Type, byte> types;

        byte typeID;
        byte[] buffer;

        public Question newQuestion { get; private set; }

        static NewQuestionPacket()
        {
            types = new Dictionary<Type, byte>();

            types.Add(typeof(MusicQuestion), 0);
            types.Add(typeof(ImageQuestion), 1);
            types.Add(typeof(TextQuestion), 2);
        }

        public NewQuestionPacket(Question question)
        {
            typeID = types[question.GetType()];
            buffer = question.buffer;
        }

        public NewQuestionPacket() { }

        public override void Serialize(NetOutgoingMessage outmsg)
        {
            outmsg.Write(typeID);
            outmsg.Write(buffer.Length);
            outmsg.Write(buffer);
        }

        public override void Deserialize(NetIncomingMessage inc)
        {
            typeID = inc.ReadByte();
            int len = inc.ReadInt32();
            buffer = inc.ReadBytes(len);

            var type = types.ElementAt(typeID).Key;
            newQuestion = (Question)Activator.CreateInstance(type, buffer);
        }
    }
}

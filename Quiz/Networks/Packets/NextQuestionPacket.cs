﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lidgren.Network;

namespace Quiz.Networks
{
    public class NextQuestionPacket : Packet
    {
        public bool request { get; private set; }

        public NextQuestionPacket(bool request)
        {
            this.request = request;
        }

        public NextQuestionPacket() { }

        public override void Serialize(NetOutgoingMessage outmsg)
        {
            outmsg.Write(request);
        }

        public override void Deserialize(NetIncomingMessage inc)
        {
            request = inc.ReadBoolean();
        }
    }
}

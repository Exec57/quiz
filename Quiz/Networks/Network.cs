﻿using Lidgren.Network;
using Quiz.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Quiz.Networks
{
    public static partial class Network
    {
        const string appIdentifier = "QuizGame";

        static NetPeerConfiguration config;
        static NetPeer peer;
        static Dictionary<Type, Action<Packet>> actions;
        static Dictionary<Type, byte> packetTypes;
        static NetConnectionStatus connectionStatus;
        static NetConnectionStatus lastConnectionStatus;

        public static bool IsServer { get => peer != null && peer is NetServer && peer.Status == NetPeerStatus.Running; }
        public static bool IsClient { get => peer != null && peer is NetClient && peer.Status == NetPeerStatus.Running; }
        public static bool IsValid { get => IsServer || IsClient; }
        public static bool IsConnecting { get => IsClient && connectionStatus == NetConnectionStatus.InitiatedConnect; }
        public static bool IsNoConnection { get => IsServer && peer.Connections.Count == 0; }

        public static event Action<NetConnectionStatus, NetConnectionStatus> OnStatusChanged;

        static Network()
        {
            actions = new Dictionary<Type, Action<Packet>>();

            packetTypes = new Dictionary<Type, byte>();
            var types = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.BaseType == typeof(Packet)).ToArray();
            for (byte i = 0; i < types.Length; i++)
                packetTypes.Add(types[i], i);
        }

        static void Init()
        {
            config = new NetPeerConfiguration(appIdentifier);

            config.DisableMessageType(NetIncomingMessageType.ConnectionLatencyUpdated);
            config.DisableMessageType(NetIncomingMessageType.DebugMessage);
            config.DisableMessageType(NetIncomingMessageType.DiscoveryRequest);
            config.DisableMessageType(NetIncomingMessageType.DiscoveryResponse);
            config.DisableMessageType(NetIncomingMessageType.Error);
            config.DisableMessageType(NetIncomingMessageType.ErrorMessage);
            config.DisableMessageType(NetIncomingMessageType.NatIntroductionSuccess);
            config.DisableMessageType(NetIncomingMessageType.Receipt);
            config.DisableMessageType(NetIncomingMessageType.UnconnectedData);
            config.DisableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            config.DisableMessageType(NetIncomingMessageType.WarningMessage);
        }

        public static bool Host(int port)
        {
            try
            {
                Init();

                config.Port = port;
                config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
                config.MaximumConnections = 1;

                peer = new NetServer(config);
                peer.Start();

                Task.Run(Listen);

                return true;
            }
            catch
            {
                peer = null;
            }

            return false;
        }

        public static void Connect(string ip, int port)
        {
            Init();

            peer = new NetClient(config);
            peer.Start();
            peer.Connect(ip, port);

            Task.Run(Listen);
        }

        static Task Listen()
        {
            NetIncomingMessage inc;

            while (IsValid)
            {
                while ((inc = peer.ReadMessage()) != null)
                {
                    switch (inc.MessageType)
                    {
                        case NetIncomingMessageType.ConnectionApproval:
                            inc.SenderConnection.Approve();
                            break;

                        case NetIncomingMessageType.StatusChanged:

                            connectionStatus = (NetConnectionStatus)inc.ReadByte();
                            OnStatusChanged?.Invoke(connectionStatus, lastConnectionStatus);
                            lastConnectionStatus = connectionStatus;

                            break;

                        case NetIncomingMessageType.Data:

                            var type = packetTypes.Keys.ElementAt(inc.ReadByte());
                            var packet = (Packet)Activator.CreateInstance(type);
                            packet.Deserialize(inc);
                            actions[type].Invoke(packet);

                            break;
                    }
                }

                peer.Recycle(inc);
            }

            peer = null;
            return null;
        }

        public static void Stop()
        {
            if (peer != null)
                peer.Shutdown("");
            OnStatusChanged = null;
        }

        public static void SendPacket(Packet packet, bool callback = false)
        {
            if (!IsValid)
            {
                actions[packet.GetType()].Invoke(packet);
                return;
            }

            var outmsg = peer.CreateMessage();

            outmsg.Write(packetTypes[packet.GetType()]);
            packet.Serialize(outmsg);

            if (peer is NetServer server)
                server.SendToAll(outmsg, NetDeliveryMethod.ReliableOrdered);
            else if (peer is NetClient client)
                client.SendMessage(outmsg, NetDeliveryMethod.ReliableOrdered);

            if (callback)
                actions[packet.GetType()].Invoke(packet);
        }

        public static void OnReceivedPacket<T>(Action<T> action) where T : Packet
        {
            if (actions.ContainsKey(typeof(T)))
                actions.Remove(typeof(T));

            actions.Add(typeof(T), new Action<Packet>(p => action((T)p)));
        }
    }
}
